import React from 'react';
import Module from './Module';

export default function App() {
  return <div>
    Main app
    <Module />
  </div>
}